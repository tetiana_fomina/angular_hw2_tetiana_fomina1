import {env} from './.env';

export const environment = {
  production: false,
  version: env.npm_package_version + '-dev',
  serverUrl: 'http://localhost:3000/api',
  socketUrl: 'http://localhost:4001/',
  defaultLanguage: 'en-US'
};
