import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { BoardsModule } from './boards/boards.module';
import { BoardModule } from './board/board.module';
import { ShellModule } from './shell/shell.module';
import { AboutModule } from './about/about.module';
import { RecoveryModule } from './recovery/recovery.module';
import { ListModule } from './list/list.module';
import { CardModule } from './card/card.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    CoreModule,
    SharedModule,
    ShellModule,
    BoardsModule,
    BoardModule,
    AboutModule,
    RecoveryModule,
    CardModule,
    ListModule,
    AppRoutingModule 
  ],
  declarations: [AppComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
